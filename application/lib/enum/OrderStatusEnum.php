<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/6/006
 * Time: 22:23
 */

namespace app\lib\enum;


class OrderStatusEnum
{
    //待支付
    const UNPAID = 1;
    //已支付
    const PAID = 2;
    //已发货delivered
    const DELIVERED = 3;
    //已支付，但是库存不足
    const PAID_BUT_OUT_OF = 4;
}