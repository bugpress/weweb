<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/28/028
 * Time: 18:22
 */

namespace app\lib\exception;


class TokenException extends BaseException
{
    public $code = 401;
    public $msg = 'Token已过期或无效Token';
    public $errorCode = 10001;
}