<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/4/24/024
 * Time: 22:58
 */

namespace app\lib\exception;


class ForbiddenException extends BaseException
{
   public $code = 403;
   public $msg = '权限不够';
   public $errorCode = 10001;
}