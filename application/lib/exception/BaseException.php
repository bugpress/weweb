<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/25/025
 * Time: 14:09
 */

namespace app\lib\exception;
use think\Exception;
use Throwable;

class BaseException extends Exception
{
    public $code = 400;
    public $msg = '参数错误';
    public $errorCode = 1000;

    public function __construct($params = [])
    {
        if(!is_array($params)){
            return ;
        }
    }
}