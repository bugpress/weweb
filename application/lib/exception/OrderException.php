<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/4/26/026
 * Time: 21:36
 */

namespace app\lib\exception;


class OrderException extends BaseException
{
    public $code = 404;
    public $msg = '订单不存在，请检查id';
    public $errorCode = 80000;
}