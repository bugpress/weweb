<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/4/004
 * Time: 22:18
 */

namespace app\lib\exception;


class SuccessMessage
{
   public $code = 201;
   public $msg = 'ok';
   public $errorCode = 0;
}