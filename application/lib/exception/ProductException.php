<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/27/027
 * Time: 15:43
 */

namespace app\lib\exception;


class ProductException extends BaseException
{
    public $code = 404;
    public $msg = '指定商品不存在，请检查商品ID';
    public $errorCode = 20000;
}