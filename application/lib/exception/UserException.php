<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/4/24/024
 * Time: 20:54
 */

namespace app\lib\exception;


class UserException extends BaseException
{
   public $code = 404;
   public $msg = '用户不存在';
   public $errorCode = 60000;
}