<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/25/025
 * Time: 16:50
 */

namespace app\lib\exception;


class ParameterException extends BaseException
{
    public $code = 400;
    public $msg = '参数错误';
    public $errorCode = 10000;
}