<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/26/026
 * Time: 17:15
 */
//自动加载，配置资源url，图片要放在public下面，才能被自动加载
//直接使用配置项config('setting.img_prefix')
return [
    'img_prefix' => 'http://wcweb.com/images',
    'token_expire_in' => 7200
];