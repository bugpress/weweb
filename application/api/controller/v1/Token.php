<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/27/027
 * Time: 17:29
 */

namespace app\api\controller\v1;


use app\api\service\AppToken;
use app\api\service\UserToken;
use app\api\validate\AppTokenGet;
use app\api\validate\TokenGet;
use app\api\Service\Token as TokenService;
use app\lib\exception\ParameterException;

class Token
{
    /**
     * 获取令牌
     * @param string $code
     * @return array
     */
    public function getToken($code='')
    {
        (new TokenGet())->goCheck();
        $ut = new UserToken($code);
        $token = $ut->get($code);
        return [
            'token' => $token
        ];
    }

    /**
     * 第三方获取令牌
     * @param string $ac
     * @param string $se
     * @return array
     */
    public function getAppToken($ac='',$se='')
    {
//        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
//        header('Access-Control-Allow-Methods: GET');
        (new AppTokenGet())->goCheck();
        $app = new AppToken();
        $token = $app->get($ac,$se);
        return [
          'token' => $token
        ];
    }
    /**
     * @param string $token
     * @return array
     * @throws ParameterException
     */
    public function verifyToken($token=''){
        if(!$token){
            throw new ParameterException([
                'token不允许为空'
            ]);
        }
        $valid = TokenService::verifyToken($token);
        return [
            'isValid' => $valid
        ];
    }
}