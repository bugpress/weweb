<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/4/25/025
 * Time: 21:34
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\validate\IDMustBePositiveInt;
use app\api\validate\OrderPlace;
use app\api\validate\PagingParameter;
use app\lib\enum\ScopeEnum;
use app\lib\exception\ForbiddenException;
use app\lib\exception\OrderException;
use app\lib\exception\SuccessMessage;
use app\lib\exception\TokenException;
use think\Controller;
use app\api\service\Token as TokenService;
use app\api\service\Order as OrderService;
use app\api\model\Order as OrderModel;
use app\api\service\Token;
class Order extends BaseController
{
    //用户在选择商品后，向API提交商品的相关信息
    //API在接收消息后，需要检查订单相关商品的库存量
    //有库存把订单数据存入数据库，下单成功，客户端可以支付
    //调用支付接口进行支付
    //还需再次进行库存量检测
    //服务器可以调用微信的支付接口进行支付
    //小程序根据服务器返回的结果拉起微信支付
    //微信会返回一个支付的结果
    //库存量的检测
    //成功：进行库存量的扣除，失败：返回一个支付失败的结果

    //前置方法做权限校验
    protected $beforeActionList = [
        'checkExclusiveScope'=>['only'=>'placeOrder'],
        'checkPrimaryScope'=>['only'=>'getSummaryByUser'],
    ];


    //订单详情接口
    public function getDetail($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $orderDetail = OrderModel::get($id);
        if(!$orderDetail)
        {
            throw new OrderException();
        }
        return $orderDetail->hidden(['prepay_id']);
    }

    //提交支付信息
    public function placeOrder()
    {
        (new OrderPlace())->goCheck();
        //接收参数
        $products = input('post.products/a');// /a是获取数组参数
        $uid = TokenService::getCurrentUid();//拿到用户的uid
        $order = new OrderService();
        $status = $order->place($uid,$products);
        return $status;
    }

    /**
     * 根据用户id分页获取订单列表
     * @param int $page
     * @param int $size
     * @return array
     */
    public function getSummaryByUser($page=1,$size=15)
    {
        (new PagingParameter())->goCheck();
        //获得用户id
        $uid = Token::getCurrentUid();
        $pagingOrders = OrderModel::getSummaryByUser($uid,$page,$size);
        if(!$pagingOrders->isEmpty()){
            return [
                'data'  =>  [],
                'current_page'  => $pagingOrders->getCurrentPage()
            ];
        }
        $data = $pagingOrders->hidden(['snap_items','snap_address','prepay_id'])->toArray();
        return [
            'data'  =>  $data,
            'current_page'  => $pagingOrders->getCurrentPage()
        ];

    }

    /**
     * 获取全部订单简要信息
     * @param int $page
     * @param int $size
     * @return array
     */
    public function getSummary($page=1,$size=20)
    {
        (new PagingParameter())->goCheck();
        $pagingOrders = OrderModel::getSummaryByPage($page,$size);
        if($pagingOrders->isEmpty())
        {
            return [
                'current_page' => $pagingOrders->currentPage(),
                'data' =>[]
            ];
        }
        $data = $pagingOrders->hidden(['snap_items','snap_address'])->toArray();
        return [
            'current_page' => $pagingOrders->currentPage(),
            'data' => $data
        ];
    }

    /**
     * 微信模板消息
     * @param $id
     * @return SuccessMessage
     */
    public function delivery($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $order = new OrderService();
        $success  = $order->delivery($id);
        if($success){
            return new SuccessMessage();
        }
    }


}