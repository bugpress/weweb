<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/6/006
 * Time: 14:15
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\service\WxNotify;
use app\api\validate\IDMustBePositiveInt;
use app\api\service\Pay as PayService;
use app\api\validate\pagingParameter;
use app\api\service\Token;

class Pay extends BaseController
{
    //前置方法做权限校验
    protected $beforeActionList = [
        'checkExclusiveScope'=>['only'=>'placeOrder']
    ];
    public function getPreOrder($id='')
    {
        (new IDMustBePositiveInt())->goCheck();
        $pay = new PayService($id);
        return $pay->pay();
    }
    public function receiveNotify()
    {
        //检测库存量
        //更新订单状态
        //减少库存
        //成功处理，返回成功的消息，否则，返回没有成功的处理
        //post:xml格式
        //调用微信回调
        $notify = new WxNotify();
        $notify->Handle();
    }
}