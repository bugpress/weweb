<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/24/024
 * Time: 21:41
 */

namespace app\api\controller\v1;
use app\api\validate\IDMustBePositiveInt;
use app\lib\exception\BannerMissException;
use app\api\model\Banner as BannerModel;
use think\Exception;
use think\Model;

class Banner extends Model
{
    public function getBanner($id)
    {
        //AOP face to section
        (new IDMustBePositiveInt())->goCheck();
        //static transfer the model
        //$banner = BannerModel::with('items')->find($id);//查询关联模型的数据
        $banner = BannerModel::getBannerById($id);
        if(!$banner)
        {
            //throw new Exception('内部错误');
           throw new BannerMissException();
        }
        return $banner;
    }
}