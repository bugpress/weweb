<?php

namespace app\api\controller\v1;


use app\api\validate\IDCollection;
use app\api\model\Theme as ThemeModel;
use app\api\validate\IDMustBePositiveInt;
use app\lib\exception\ThemeException;

class Theme
{
    /**
     * @param string $ids
     * @return false|\PDOStatement|string|\think\Collection
     * @throws ThemeException
     */
   public function getSimpleList($ids='')
   {
       (new IDCollection())->goCheck();
       //return 'success';
        $ids = explode(',',$ids);//explode() 函数把字符串打散为数组。
        $result = ThemeModel::with('topicImg','headImg')->select($ids);
        if($result->isEmpty()){
            throw new ThemeException();
        }
        return $result;
   }

    /**
     * @param $id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws ThemeException
     */
   public function getComplexOne($id)
   {
       (new IDMustBePositiveInt())->goCheck();
       $theme = ThemeModel::getThemeWithProducts($id);
       if(!$theme){
           throw new ThemeException();
       }
       return $theme;
   }
}
