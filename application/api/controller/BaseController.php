<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/4/25/025
 * Time: 22:17
 */

namespace app\api\controller;


use think\Controller;
use app\api\service\Token as TokenService;
class BaseController extends Controller
{
    protected function checkPrimaryScope(){
        TokenService::needPrimaryScope();
    }

    protected function checkExclusiveScope(){
        TokenService::needExclusiveScope();
    }
}