<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/24/024
 * Time: 23:02
 */

namespace app\api\validate;

class IDMustBePositiveInt extends BaseValidate
{
    protected $rule = [
        'id' => 'require|isPositiveInteger',
    ];

    protected $message=[
        'id' => 'id必须是正整数'
    ];
}