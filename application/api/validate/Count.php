<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/27/027
 * Time: 15:26
 */

namespace app\api\validate;


class Count extends BaseValidate
{
    protected $rule = [
      'count' => 'isPositiveInteger|between:1,15'
    ];
}