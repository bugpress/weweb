<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/28/028
 * Time: 16:43
 */

namespace app\api\validate;


class AppTokenGet extends BaseValidate
{
   public $rule = [
     'ac' => 'require|isNotEmpty',
     'se' => 'require|isNotEmpty'
   ];
}