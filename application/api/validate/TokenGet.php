<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/27/027
 * Time: 20:23
 */

namespace app\api\validate;


class TokenGet extends BaseValidate
{
    protected $rule = [
        'code' => 'require|isNotEmpty'
    ];

    protected $message = [
        'code' => '你可能在做梦哦'
    ];
}