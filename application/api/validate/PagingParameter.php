<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/9/009
 * Time: 20:13
 */

namespace app\api\validate;


class pagingParameter extends BaseValidate
{
    protected $rule = [
        'page' => 'isPositiveInteger',
        'size' => 'isPositiveInteger'
    ];
    protected $message = [
        'page' => '分页参数必须是正整数',
        'size' => '分页参数必须是正整数'
    ];
}