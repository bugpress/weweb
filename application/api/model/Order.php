<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/6/5/005
 * Time: 23:06
 * by jing<714195347@qq.com>
 */

namespace app\api\model;


class Order extends BaseModel
{
    protected $hidden = ['user_id','delete_time','update_time'];
    //自动写入时间戳,要在model的情况下
    protected $autoWriteTimestamp = true;
    //读取器
    public function getSnapItemsAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return json_encode($value);
    }
    public function getSnapAddressAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return json_encode($value);
    }
    //自定义时间字段
//    protected $createTime = 'create_timestamp';
    public static function getSummaryByUser($uid,$page=1,$size=15)
    {
        $pagingData = self::where('user_id','=',$uid)->order('create_time desc')->paginate($size,true,['page'=>$page]);
        return $pagingData;
    }

    public static function getSummaryByPage($page=1,$size=20)
    {
        $pagingData = self::order('create_time desc')->paginate($size,true,['page'=>$page]);
        return $pagingData;
    }
}