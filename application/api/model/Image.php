<?php

namespace app\api\model;

use think\Model;

class Image extends BaseModel
{
    protected $hidden = ['id','from','delete_time','update_time'];

    //读写器
    public function getUrlAttr($value, $data)
    {
        return $this->prefixImgUrl($value,$data);
    }
}
