<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/29/029
 * Time: 10:38
 */

namespace app\api\model;


class ProductImage extends BaseModel
{
    protected $hidden = ['img_id','delete_time','product_id'];

    public function imgUrl()
    {
        return $this->belongsTo('Image','img_id','id');
    }
}