<?php
/**
 * Created by PhpStorm.
 * User: Administratorjing
 * Date: 2018/3/25/025
 * Time: 13:33
 */
namespace app\api\model;


use think\Db;
use think\Model;

class Banner extends BaseModel
{
    //隐藏字段
    protected $hidden = ['delete_time','update_time'];
    public function items()
    {
        //一对多的关系，关联模型
        return $this->hasMany('BannerItem','banner_id','id');
    }
    public static function getBannerById($id)
    {
        $banner = self::with(['items','items.img'])->find($id);
        return $banner;
    }
}